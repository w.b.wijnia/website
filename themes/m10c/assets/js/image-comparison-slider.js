jQuery(document).ready(($) => {
  var imageComparisonSliders = $('.image-comparison-slider');
  for (var imageComparisonSlider of imageComparisonSliders) {
    initializeComparisonSlider($, $(imageComparisonSlider));
  }
});

function initializeComparisonSlider($, imageComparisonSlider) {
  var body = $(document.body);
  var id = imageComparisonSlider.attr('id');
  var positionLeft = imageComparisonSlider.position().left;
  var width = imageComparisonSlider.width();

  var comparisonOverlay = $(`#${id} .image-comparison-slider-overlay`);
  var comparisonSlider = $(`#${id} .image-comparison-slider-slider`);

  comparisonSlider.on('mousedown', onSlideStartFn);
  comparisonSlider.on('touchstart', onSlideStartFn);

  function onSlideMoveFn(event) {
    var offsetToEdge = 4;
    var sliderPosition = event.pageX - positionLeft - window.pageXOffset;
    if (sliderPosition < offsetToEdge) {
      sliderPosition = offsetToEdge;
    } else if (sliderPosition >= (width - offsetToEdge)) {
      sliderPosition = width - offsetToEdge;
    }

    comparisonOverlay.css('right', (width - sliderPosition) + 'px');
    comparisonSlider.css('left', (sliderPosition - 8) + 'px');
  }

  function onSlideStartFn(event) {
    event.preventDefault();

    // attach all appropriate events!
    body.on('mouseup', onSlideEndFn);
    body.on('touchend', onSlideEndFn);

    body.on('mousemove', onSlideMoveFn);
    body.on('touchmove', onSlideMoveFn);
  }

  function onSlideEndFn(event) {
    event.preventDefault();

    // remove all events
    body.off('mouseup', onSlideEndFn);
    body.off('touchend', onSlideEndFn);

    body.off('mousemove', onSlideMoveFn);
    body.off('touchmove', onSlideMoveFn);
  }
}
