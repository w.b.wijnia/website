jQuery(document).ready(($) => {
  var imageGalleries = $('.image-gallery');
  for (var imageGallery of imageGalleries) {
    initializeGallery($, $(imageGallery));
  }
});

function initializeGallery($, imageGallery) {
  var id = imageGallery.attr('id');

  // initialize the obj we track all values on
  var gallery = {
    slideCount: $(`#${id} .image-gallery-slides-list li`).length,
    slideIndex: 0,

    width: imageGallery.width(),
  };

  // set the widths for all underlying components
  $(`#${id} .image-gallery-slide`).css('width', gallery.width + 'px');
  $(`#${id} .image-gallery-slides-list`).css('width', (gallery.width * gallery.slideCount) + 'px');

  $(`#${id} .image-gallery-left`).click(function () {
    performSlide('back');
  });

  $(`#${id} .image-gallery-right`).click(function () {
    performSlide('forward');
  });

  function performSlide(direction) {
    var nextSlideIndex = direction === 'back'
      ? gallery.slideIndex - 1
      : gallery.slideIndex + 1;

    if (nextSlideIndex <= -1) {
      // loop back to the last index
      nextSlideIndex = gallery.slideCount - 1;
    } else if (nextSlideIndex >= gallery.slideCount) {
      // loop back to the first entry
      nextSlideIndex = 0;
    }

    var margin = gallery.width * nextSlideIndex;

    $(`#${id} .image-gallery-slides-list`).css('transform', 'translate3d(-' + margin + 'px,0px,0px)');

    gallery.slideIndex = nextSlideIndex;
  }
}
