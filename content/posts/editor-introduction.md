---
title: "Editors: introduction"
date: 2021-05-07T21:41:12+02:00
draft: false
tags: ["editor"]
avatar: "/images/site/blog.png"
---

Strategy games tend to use a common format to construct their terrain. This article will dive into that format to provide a foundation to understand the context of future articles with regard to the type of editor. 

For this series I'll use Supreme Commander: Forged Alliance as an example. For those that are unaware, there is an interesting [graphics study](http://www.adriancourreges.com/blog/2015/06/23/supreme-commander-graphics-study/) made by Adrian Courrèges that covers the entire graphics pipeline in more depth. 

### Examples

We'll use two maps as examples to explain the techniques that are applied. The reason for two examples is that they are build from a different perspective. We'll dive into this difference when we talk about decals. 

![](/images/editors-introduction/examples.png)
_Aalhaven (left) made by the community member biass and Kaali (right) made by me_

We'll focus primarily on Aalhaven as that map uses the classic techniques.

### Heightmap

A heightmap is a single channel image with 16 bits per pixel. With a single channel you can only create shades of grey. Instead of having 50 shades of grey an image with 16 bits per pixels can create up to 65536 different shades. We use this information to push the terrain outwards on the up axis of the game. This allows an artist to create mountains, hills, plateaus, ramps, cliffs and other naturally occurring elements that you'd expect in a terrain. 

{{< image-comparison-slider id="heightmapSlider" left="/images/editors-introduction/heightmap-example/02-without.png" right="/images/editors-introduction/heightmap-example/03-with.png" >}}

In Supreme Commander a heightmap determines the gameplay of a map. All the other elements that we'll discuss can help communicate the gameplay to the player but they have no influence on it. The pathability is determined by the slope of the terrain and projectiles from units firing at each other can collide with the terrain. Therefore units with various firing arcs are more or less powerful depending on where you take the battle. In other games the heightmap may also influence what you can see through the fog of war. 

### Texture splatting

One technique that Supreme Commander uses to add fidelity to the terrain is painting or splatting textures onto it. During rendering a series of masks determine what textures should appear where. To get intuition as to what this entails it is best to just show it. 

{{< image-gallery dir="/images/editors-introduction/stratums-example/" id="splattingGallery" >}}

Each stratum layer has a mask (left sub-image), an albedo map (center sub-image) and a normal map (right sub-image). The mask determines where these properties are painted on the terrain. Black indicates no covrage, white indicates a complete coverage. A higher stratum layer will cover any lower stratum layers.

Fun fact: in Supreme Commander textures on the terrain start to appear once the value is higher than 128. This is because in the rendering sequence they apply this little code to the mask value: `mask = mask * 2 - 1`. Because of this you need to have at least a value higher than `0.5` in order to get some coverage.

### Decals

Another technique to add fidelity is through decals. Decals are textures that are projected onto the terrain. They can have any orientation - some may stretch the texture however. They can project the same properties as the stratum layers can provide, including albedo, normals and specular values. Decals are generally not seamless, in contrast with the stratum layers. Therefore they are good in adding detail or to break the repetitive nature of stratum layers.

{{< image-comparison-slider id="aalhavenDecalSlider" left="/images/editors-introduction/decals-example/01-without.png" right="/images/editors-introduction/decals-example/02-with.png" >}}

In practice decals are so essential that a terrain with and without decals is the difference between day and night. In Aalhaven Biass choose decals from a wide range of available decals and placed them individually. In Kaali however the map uses in total three decals that are procedurally generated. These map-wide decals provide a sense of detail that is not possible when placing decals manually.

{{< image-comparison-slider id="kaaliDecalSlider" left="/images/editors-introduction/decals-kaali-example/01-without.png" right="/images/editors-introduction/decals-kaali-example/02-with.png" >}}

And a slide show to make it easier to see the impact of the individual map-wide decals. Each has a specific role to perform:
 - Lighting: Adds in ambient occlusion and shadows for the terrain.
 - Normals: These are world-space normals that introduce peaks and sharp ridges.
 - Albedo: Adds in significant detail to prevent the repetition of the seamless layers.

{{< image-gallery dir="/images/editors-introduction/decals-kaali-individual-example/" id="decalsKaaliIndividualGallery" >}}

With this information you are prepared to understand the other articles. We'll talk about time, expertise, accessibility and flexibility. We'll use these examples in combination with an editor to talk about features that a next-gen editor should have.