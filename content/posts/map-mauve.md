---
title: "Mauve: a community collaboration"
date: 2021-03-07T22:25:44+01:00
draft: false
tags: ["map", "forged-alliance", "community"]
avatar: "/images/site/blog.png"
---

![preview-1](/images/mauve/preview-1.png)

One of the three maps as a result of a [community collaboration](https://forum.faforever.com/topic/1214/a-community-effort-on-a-map-layout). The idea was to bring the community together by allowing them to submit a design. If the design met the criteria and was reasonable as a whole I'd consider turning the design into an actual map. The design of Mauve was made by Sebastian Brinkman (Leto_II). 

![preview-2](/images/mauve/preview-2.png)

Designing a map with the intent to make it yourself can prevent you from being experimental. As it is easy to tell yourself something is quite hard to do. And by telling yourself you may change the design. In this case I didn't design it myself and therefore there could be technical bits in the map that appear hard to do.

![preview-2](/images/mauve/shore-without.png)

One particular feature I was not looking forward to was creating shore lines.
 - They should follow the shore, but not too perfect
 - They should appear on the sandy parts of the shore and nowhere else

The last feature is about making the right masks, extracting the right features from the terrain. By using a slope and height selector I could select area's that are part of the beach, but not particularly steep as that would indicate rock formations. Combine that with excluding the center and you have the area's that you'd like to make shore lines for.

Creating the shore lines themselves turned out to be a breeze. By using the mask of I could extract the parts of the terrain that feature shore lines. By terracing, equalizing along with a slope selector we end it with a pattern that represents what we need.

![preview-2](/images/mauve/shore-with.png)

Add post processing to that and we have shorelines!

Another interesting use case for this is generating contour lines or _isohypses_ from a map. When applying it to terrain it is called a [relief map](https://wiki.openstreetmap.org/wiki/Relief_maps). A relief map creates a visual representation of how steep / bumpy terrain is. They didn't end up in the map after a bit of a discussion with other community members about, but they did end up in the overview.

![relief map](/images/mauve/overview.png)

There is more to this map but that is for another post. For more information about the map you can look at the [repository](https://gitlab.com/supreme-commander-forged-alliance/maps/mauve) or the [topic](https://forum.faforever.com/topic/1214/a-community-effort-on-a-map-layout) the design originated from. The map is available in the [FAF](https://www.faforever.com/) map vault and the [LOUD](https://www.moddb.com/mods/loud-ai-supreme-commander-forged-alliance) map library.
