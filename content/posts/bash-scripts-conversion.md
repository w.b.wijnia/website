---
title: "Bash scripting: converting images"
date: 2021-03-07T22:32:29+01:00
tags: ["automation", "bash", "workflow", "pipeline"]
avatar: "/images/site/blog.png"
draft: false
---

A common task one has to perform while making content is image conversion - changing the encoding of an image. As an example: converting a file to the Direct Drawing Surface (DDS) format that is commonly used in games. We'll look into how we can approach to automate such a task by using bash scripts and open source tools available online.

### Prerequisites

Throughout this post we'll use the following tools:
 - [Image Magick](https://imagemagick.org/index.php)

And for windows users you'll need a console that supports bash:
 - [Git Bash](https://git-scm.com/downloads)

Make sure that **Image Magick** is in your path environment variable. You can check this in your bash shell by running `magic --version`.

### Converting one file

For converting a single file we'll use the tool **Image Magick**. This is one of those tools that is so overly populated with features that you could easily make a living by just understanding the tool. Luckily for us we don't need to understand all of the tool for this small task.

<script src="https://gitlab.com/w.b.wijnia/website/-/snippets/2090270.js"></script>

A small example that isn't particularly flexible but it does the job of showing the right syntax. We define an input file, We define an output file and then we make a call to our image editing tool. The tool in question will load the input, convert it with our desired compression and store it in the output location. 

<script src="https://gitlab.com/w.b.wijnia/website/-/snippets/2090271.js"></script>

A flexible example that uses the first argument of the script as its input file. As a first step in automation it performs a similar task to the former script, but now we don't have to define our output anymore: we take off the extension of our input, append the correct extension and make that our output.

### Converting a folder of files

For converting a batch of files we'll use more features of our scripting language. We'll apply the previous procedures in a different context.

<script src="https://gitlab.com/w.b.wijnia/website/-/snippets/2090272.js"></script>

Instead of providing a file, we provide a directory. The for loop in a bash script can iterate over each file in the directory. A file can be anything - including another directory. We specifically ignore directories. Each entry that we process is therefore a file. We use that to determine the input and output of the conversion process as we did before.

Essentially we look at every file in a folder that we provide and convert that file. Suddenly the script is a lot more powerful - we can now apply the procedure to entire folders while we can go work on some other task!

### There is more

This is just the start. There is a lot more that we can to automate this task even further:
 - Converting a folder of folders and files
 - Gathering all output in an output directory
 - Checking the output directory for more recent files and skipping conversion if they are
 - Automatically unpack and convert assets in zip files
 - Cleaning up unpacked zip files
 - Adding a parameter for the type of compression
 - Adding a parameter for the type of output in general
 - ...

The limit is the combination between your imagination and the open source tools available. 