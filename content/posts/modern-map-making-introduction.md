---
title: "Modern map making: overview"
date: 2021-05-07T2:11:01+02:00
draft: true
tags: ["map", "forged-alliance", "community"]
avatar: "/images/site/blog.png"
---

### START OF TODO

This first part should be about the technical depth that is required to effectively make maps. The aim of this series is to overcome this depth via building blocks.

Three core elements:
 - time (think about all manual labour, placing individual decals)
 - expertise (think about World Machine)
 - accessibility (think about the costs / requirements for world machine)
 - flexibility (think about changes)

Core elements:
 - Time
 - Expertise
 - Accessibility
 - Flexibility

Aim: create high quality maps


### END OF TODO

Building content for a game is as smooth as the editor you use to construct it with. This article represents my view on what an editor should be. We'll discuss a few existing editors and compare them to find common grounds. The aim is to be able to create high quality content effectively with one particular element above all others: the community surrounding the game should be able to do it just as effectively.

When we review an editor we'll look at four perspectives:
 - Time: how much time does it cost to make a map A to Z?
 - Expertise: how much knowledge does it require to make a map?
 - Accessibility: how easy is it for people to incorporate their ideas into a map?
 - Flexibility: how easy is it to make adjustments after receiving feedback?

### Comparing editors

#### Halo 2

Mediocre amount of time, high expertise, low acessibility, high flexibility

#### Supreme Commander: GPG Editor

Requires a lot of time, expertise, mediocre accessibility and low flexibility.

#### Supreme Commander: FAF Editor

Decrease of time, expertise and increase of accessibility but low flexibility.

### About effective workflows

Humble on about using various tools to accomplish a task

make a comparison to video editing

### Common grounds

#### No production format

#### Little flexibility

####



### An ideal editor



#### Accessible format

An accessibile format can be seen from various perspectives. We'll talk about:
 - binary blops
 - Development format vs production format
 - accompanying tools



Any unneccessary binary file is a no-go. Performance-wise there is no need for them these days. But more important: it hampers with the content makers to create an effective workflow. As an example we'll take the Supreme Commander format. You will find the stratum masks inside the binary map file of Supreme Commander. A consequence of this is that if I use external software to construct the masks I'd need to manually import them through the editor. From another perspective: if I want to use external software to change the masks I'd need to manually export, change it, and then import it again. The mandatory manual labour involved in this process will break any effective workflow.

Ideally when editing the map there should not 

Multiple files that contain props information to split manual work from procedural work, combined during baking

Allows for additional tools to be made to improve accessibility, flexibility and reduce time and expertise

#### Building blocks

Allows for people to create high-quality content with less time, expertise, high amounts of accessibility (they can make their own blocks too) and high amounts of flexibility (select multiple blocks and move them around)

#### Baking a map

The core of a strategy game is its terrain. It determines the majority of the aesthetics onscreen and the gameplay that is possible to the players. As an artist, as a developer or as a community contributor being able to manipulate this terrain effectively during development is key.

### About terrains in general

There are two core approaches to creating a terrain:
 - (1) A heightmap based approach.
 - (2) A geometry based approach.

Approach (1) is common for a lot of older games. A few examples are Supreme Commander[1] or Beyond All Reason (BAR)[2] that uses the [spring engine](https://springrts.com/). In its core it is a heightmap that supports various stratum layers. Each stratum layer has various textures with various properties and has a mask that depicts where the these properties should be visible. 

In practice this boils down to manual labour in the form of painting on the terrain in an editor. An alternative is using procedural software such as Gaea, World Machine or World Creator to speed up parts of the process of making a map. 

![supreme commander and BAR](/static/images/modern-map-making/supreme-bar.png)

Approach (2) is common for more recent games that create environments that are partially or completely geometry based. Two examples are Supreme Commander 2[3] and the Halo Wars[4] series. In the former the environment is literally geometry. In the latter it is still a heightmap but without the restriction of being able to more a vertex on only the y-axis. In turn it has a similar pattern with applying properties on the terrain. 

In practice this approach is a lot of manual labour. You can start with a procedurally generated terrain but those are limited to creating heightmaps. At some point you'll have to start sculpting the terrain manually to fully utilise the third dimension.

![Supreme Commander 2 and Halo Wars](/static/images/modern-map-making/supreme-halo-2.png)

In both cases there are a lot of approaches one can take to add more fidelity to the terrain. One could think of decals that are projected on the terrain. These hide the repetition of seamless textures and can add detail that would otherwise be hard to achieve. Another approach is extensive use of props to simulate formations that you could not easily create otherwise. An example is creating a hole in your terrain to simulate a waterfall and creating overhangs.

### An approach to terrains using heightmaps

A common approach to increase the fidelity of a terrain effectively is using map-wide assets. These are assets that stretch across the entire map. 



For this series I am going to focus on the first approach: creating a terrain using a heightmap. This appears limiting but it is not: you can still add a lot of fidelity and the possibilities are still almost endless. An example is adding the support of bridges to the path finding. Another example is having holes in your terrain and being able to place props to hide the hole. In turn, you can create overhangs using the props.



Specifically I am going to aim how to create an efficient workflow to create a 
In both scenarios props can be used to increase the fidelity of the terrain significantly. For this article however I do not wish to discuss 

There is a common route with making heightmap based maps for games. In its core it is about painting various stratum masks on some terrain and each stratum layer will deliver its properties based on that mask. More detail is added through decals or detail maps to hide the repetitive nature of the seamless stratum layers. 

### The problem

### Phases

#### Building

#### Baking

### Sources

[1]

[2]

[3]

[4] http://remember-ensemblestudios.com/ensemble-studios/halo-wars/halo-wars-dev-blog-archive/halo-wars-dev-blogs-page-2/