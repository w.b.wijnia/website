---
title: "Editors: Pieces and Parts"
date: 2021-05-07T21:41:12+02:00
draft: true
tags: ["editor"]
avatar: "/images/site/blog.png"
---

A common approach to creating terrains in strategy games is via a heightmap. The heightmap transforms a plane with vertices by pushing 
