---
title: "Auralian: remastering the past"
date: 2021-05-06T16:11:01+02:00
draft: false
tags: ["map", "forged-alliance", "community"]
avatar: "/images/site/blog.png"
---

![preview-1](/images/auralian/preview-1.png)

I've been making maps for a few years now and have been growing steadily in both my knowledge and capabilities. One of the first maps that I've made is `Auralian - The Core`. A small map where the 'heightmap' was a mesh in 3DS Max that I rendered with a gradient from an orthogonal top-view in order to turn it into an heightmap. For a long time I felt silly for doing that. Now that I've grown more the idea on its own wasn't that crazy. Using external software to meet your requirements is common practice. In the end it is about having an effective workflow. 

![old-style](/images/auralian/compare-B.png)

_The old version of Auralian - The Core_

![new-style](/images/auralian/compare-A.png)

_The new version of Auralian - The Core in a similar camera angle and position_

The map is gameplay wise the same. All players have similar capabilities to the original. Aesthetically the cliffs have been slightly altered to remove the unnatural linear interpolations of the mesh it originated from. On top of that more detail is introduced in the heightmap to accomodate the albedo and normal maps.

![layers-cur](/images/auralian/layers.png)

It can be hard to distinguish what the effect of various textures and their properties is in practice. To show this I've made two sliced up versions of the same camera angle and position. Each slice has less textures applied to the terrain. For the top image from left to right: `Lighting, Albedo, Normals` - `Albedo, Normals` - `Albedo` - `Vanilla Supreme Commander`. For the bottom image from left to right: `Vanilla Supreme Commander` - `Normals` - `Albedo, Normals` - `Lighting, Albedo, Normals`. The lighting texture is a combination of AO and raytraced shadows. 

![layers-cur](/images/auralian/layers-old.png)

One technical aim of this map was to create alternative rock formations. The erosion node is a key element in making the rock formations. When you apply various patterns on the cliffs before applying erosion you have a lot of influence on the output of the erosion. This isn't perfect by any means - artifacts will show up. Dealing with these requires careful tuning of parameters and typically a small blur before hand can help a lot.

![preview-2](/images/auralian/preview-2.png)

The other technical aim was creating an interesting ocean floor. In Supreme Commander these tend to receive a lot less attention than other elements of the map. The global intensifier of the erosion node tends to rip your terrain into pieces. In turn your gameplay is altered by ramps that have been introduced due to the aggressive eroding operation. It turns out that the only place where it would not affect gameplay in a significant manner is when it is applied to terrain that is underwater, creating beautiful curly shapes on the ocean floor.

![overview](/images/auralian/overview.png)

For more information about the map you can look at the [repository](hhttps://gitlab.com/supreme-commander-forged-alliance/maps/auralian-the-core). The map is available in the [FAF](https://www.faforever.com/) map vault.