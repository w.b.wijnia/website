---
title: "Scripting: introduction"
tags: ["scripting", "lua", "lua-guide", "supreme-commander"]
avatar: "/images/site/blog.png"
date: 2021-03-21T19:50:50+01:00
draft: true
---

Scripting is a practical introduction to programming. One major advantage is that you'll be quickly in the position to show your work to others. That on its own can spark motivation to continue.

In this series of scripting we'll focus on scripting in an older game called Supreme Commander: Forged Alliance. We'll start with a few basic elements of programming languages in general. This is important to understand the context of what it is that you are writing. After that we'll get practical and we'll work with example scripts that are carefully explained. Through these example scripts we'll introduce new concepts. You'll be applying these concepts through small exercises that you can apply to the provided example scripts.

We'll also cover topics that are not directly related to scripting. You need to understand these topics in order to improve. Therefore we'll also look in-depth at an Interactive Development Environment (IDE) and how Git repositories work in general. 

If you have no programming experience then it is recommended that you do not skip one or more parts of the series. The order they appear in has been carefully curated. For those that do have experience I'd recommend to atleast skim through sections you wish to skip.