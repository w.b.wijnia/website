---
title: "Scripting: variables and types"
tags: ["scripting", "lua", "lua-guide", "supreme-commander"]
avatar: "/images/site/blog.png"
date: 2021-03-21T19:50:50+01:00
draft: true
---

For a computer a piece of information is just bits with a certain interpretation. For this article we will start with what bits are and then continue with three examples as to how bits can be interpreted. This provides us with sufficient context to understand the topic of this article.

### Bits

Physically a single bit is a cell in your computer that is either charged or not charged. This entails that a bit can have two states: we can switch between an on state and an off state. A bit on its own this isn't quite interesting. We can combine bits. We call groups of 8 bits a byte. We can switch each bit of the byte individually.

This is where it becomes interesting. How many variations can we make with a given amount of bits? We will call a set of variations a sequence. Lets go make the variations!

 - 1 bit (2 variations): 0, 1
 - 2 bits (4 variations): 00, 01, 10, 11
 - 3 bits (8 variations): 000, 001, 010, 011, 100, 101, 110, 111
 - 4 bits (16 variations): 0000, 0001, 0010, 0011, 0100, 0101, 0110, 0111, 1000, 1001, 1010, 1011, 1100, 1101, 1110, 1111
 - 5 bits (32 variations): ...

A trick you can use to generate the next sequence is by taking the previous sequence and copy it twice. For the first copy you prepend a 0 to each variation and for the second copy you prepend a 1 to each variation. If you look carefully at the sequences you can already see me applying this trick:

 - 2 bits (4 variations): 00, 01, 10, 11
 - 3 bits (8 variations): (000, 001, 010, 011) (100, 101, 110, 111)

Try it yourself by making the sequence for 5 bits. If you end up with 32 unique variations then you've got it right. 

We can use this trick to provide us with some intuition. Everytime we add an additional bit we  multiply the number of variations by two. Therefore, without computing the variations, we can determine the number of variations given a number of bits:

 - 1 bit:   2               = 2
 - 2 bits:  2*2             = 4
 - 3 bits:  2*2*2           = 8
 - 4 bits:  2*2*2*2         = 16
 - 5 bits:  2*2*2*2*2       = 32
 - 6 bits:  2*2*2*2*2*2     = 64
 - 7 bits:  2*2*2*2*2*2*2   = 128
 - 8 bits:  2*2*2*2*2*2*2*2 = 256

We can generalize this with a formula to $v = 2^b$, where $b$ is the number of bits and $v$ is the number of bits. 

Any piece of memory on a computer is a large collection of bits. The more bits we have the more information we can store. As mentioned at the start of this article: for a computer everything is just a sequence of bits. What is relevant is the interpretation of those bits. Lets dive into that next.

### Example: Integral numbers

A common use case is to consider bits to be numbers. We'll start with how we can interpretate one byte. A byte has eight bits. We can add interpretation to each individual bit:

 - 0000 0001: 1
 - 0000 0010: 2
 - 0000 0100: 4
 - 0000 1000: 8
 - 0001 0000: 16
 - 0010 0000: 32
 - 0100 0000: 64
 - 1000 0000: 128

Lets go over a few examples:

 - 6:   0000 0110
 - 13:  0000 0111
 - 65:  0100 0001
 - 190: 1011 1110

Try and figure out the following numbers yourself:

 - 11, 90, 131, 211

It may take a while. A hint is to try and subtract the largest number first. As an example with 190: we can subtract 128, so that bit is on. Then we can subtract 32, 16, 8, 4 and 2. So those bits are turned on too. Any number we couldn't subtract is turned off. 

You can check yourself by applying the bit interpretations to your bit sequence. Each bit that is on adds to the results. As an example: 0000 0110 = 4 + 2 = 6.

A common example physicians like to use when explaining binary numbers in high school is the use of your hand. A hand has up to ten fingers. Each finger represents a bit. What is the maximum number of variations possible when we have 10 bits? And what would therefore be the largest number we can represent using your hands? 

### Example: ASCII

In the previous example we interpreted a sequence of bits as numbers. We can also interpret them as characters - where every sequence represents some character. Back in the days in America they standardized this with the [ASCII]() table.

### Example: Images

### Example: Memory