---
title: "Scripting in Supreme Commander: overview"
tags: ["scripting", "lua", "lua-guide"]
avatar: "/images/site/blog.png"
date: 2021-03-21T19:50:50+01:00
draft: true
---

Recently I've been giving scripting sessions to two friends on Tuesday evening. They asked me what it would take to make their own survival. I started to show them a road they could take. A month or two later and their creations are taking shape - in some form of another it is making me proud.

In general learning to script is as hard as it is to get the right information at the right time. In the case of Supreme Commander there is essentially little information. Everyone that has no other experience in programming will have to re-invent the wheel. And re-inventing the wheel is hard - but above all unneccesary.



