---
title: "Scripting in Supreme Commander: preparing your workspace"
tags: ["scripting", "lua", "lua-guide"]
avatar: "/images/site/blog.png"
date: 2021-03-21T19:50:50+01:00
draft: true
---

Throughout this article we'll go over your tools. We'll install an Interactive Development Environment (IDE) and then try to understand what it can do for you. We'll look into creating a library of assets that we can query - like creating our own encyclopedia. And last but not least we'll download the map that we'll use to create a survival in.

### Choosing an IDE
<hr>
There are so many IDE's out there these days that its hard to see the actual forest. A few examples are [Notepad++](https://notepad-plus-plus.org/downloads/), [Atom](https://atom.io/), [Sublime Text](https://www.sublimetext.com/) and [Visual Studio Code](https://code.visualstudio.com/).

In these series we'll focus on Visual Studio Code. There are various reasons for them:
 - I personally am used to using Visual Studio Code.
 - It is easy to add in code snippets.
 - It is easy to customize.
 - It is easy to use as a tool for our encyclopedia.

These reasons may apply to any of them. Therefore one is not necessarily better than the other. However, for the ease of following along I recommend you to use Visual Studio Code too.

### Learning about your IDE

Generally people do not learn about the tools that they use. Its similar to having a physical workshop with all the modern tools you imagine. While at the same time you only understand how a hammer works and last week you started getting a grip on what a screw driver does.

Throughout these series we'll keep coming back to Visual Studio Code to learn more about how it operates. The more you understand your tools, the better your development environment becomes and the easier it is to keep on working.

#### About features

Visual Studio Code is so densily feature packed that it may be hard to start working 

#### About hotkeys

It is important to learn about the tools that you're using. Hotkeys is one example of that. This section composes a small list of hotkeys that are _essential_ to know. 
 - CTRL + F: Search in file
 - CTRL + SHIFT + F: Search in workspace
 - CTRL + P: Go to file
 - CTRL + G: Go to line

Hotkeys that makes various small tasks simpler:
 - CTRL + X: Cut line
 - CTRL + C: Copy line
 - CTRL + L: Select line
 - CTLR + /: Comment line
 - ALT + Up: Move line up
 - Alt + Down: Move line down
 - CTRL + K + C: Comment selection
 - CTRL + K + U: Uncomment selection

Other useful hotkeys:
 - CTRL + B: Open / close side bar
 - CTRL + Shift + P: Show command palette

If you're not used to using hotkeys then the idea of learning to apply them is odd. We'll use the essential hotkeys throughout the scripting series. I encourage you to learn the other hotkeys as they will only benefit you in the long run.

#### A basic extension

Visual Studio Code is highly customizeable. There is a community out there that creates all kinds of extensions. One particular extension I'd like to talk about is `Peacock`.

Hold `CTRL + SHIFT + X` to open the extensions tab. Search for `Peacock` at the top. When you click on the extension a new page opens that describes the extension. Peacock allows you to color code your editor. Install the extension and hold `CTRL + SHIFT + P` to open up the commander palette. Search for `peacock` and look for `Peacock: Change to a favorite color`. 

### Building your encyclopedia

All programmers discover [Stack Overflow](https://stackoverflow.com/) at some point in their programming career. The website represents a forum - you can ask a question and various people can answer it. The answer that helped the person asking the question receives a green checkmark. If you'd ever have the same issue then the answer is right there for you to find.

Such an environment is extremely useful to quickly search for small, common questions. Sadly however Supreme Commander and its scripting environment is far from a common question. Therefore we need to create our own encyclopedia that we can query with questions and hopefully provide us with a few answers to study.

We'll construct our encyclopedia by copying code from the game into one common directory that functions as a workspace for our IDE. We can then query this workspace via `CTRL + Shift + F` - scanning every file in our workspace to find a match on our query. 

Before we start you need to decide where you will store the encyclopedia. This can be anywhere - in my case I am storing it in `C:\Develop\supreme-commander\encyclopedia`.

#### Gathering script examples

Navigate towards your Supreme Commander: Forged Alliance installation directory. And then into the gamedata folder. In my case this is located in `C:\Steam\steamapps\common\Supreme Commander Forged Alliance\gamedata`. 

In this directory we see a whole bundle of files with the `.scd` extension. That is an abbrevation for `Supreme Commander Data` and is a zip file in disguise. Copy the following files to your encyclopedia:
 - `mohodata.scd`
 - `moholua.scd`
 - `lua.scd`

Once copied change their extension to from `.scd` to `.zip` and then use your favorite compression software to unpack them. Remove the original `.scd` files from our encyclopedia. 

#### Gathering map examples

Navigate towards your Supreme Commander: Forged Alliance installation directory. And then into the maps folder. In my case this is located in `C:\Steam\steamapps\common\Supreme Commander Forged Alliance\maps`. 

In this directory are a whole bunch of maps. Particularly the campaign maps are interesting as they contain logic to determine how the mission should proceed. Copy the following folders to a maps folder in your encyclopedia:
 - `X1CA_001`
 - `X1CA_002`
 - `X1CA_003`
 - `X1CA_004`
 - `X1CA_005`
 - `X1CA_006`
 - `X1CA_TUT`

#### Gathering external repositories

You're likely still playing Supreme Commander because you're part of the [FAF](www.faforever.com) or [LOUD](https://www.moddb.com/mods/loud-ai-supreme-commander-forged-alliance) community. These communities have repositories that you can download. A repository is a large collection of scripts - similar to the `.scd` files that we extracted from the game. I recommend you to get at least the [FAF repository](https://github.com/FAForever/fa) as it has quite useful documentation. The [LOUD repository](https://github.com/LOUD-Project/Git-LOUD) can be found here.

You can download a repository by clicking on the green button that says `Code`. Select `Download as ZIP` from the dropdown menu if you do not know what the other options are. Unpack the `.zip` file into your enclyclopedia.

#### Creating a workspace

At this point you have an encyclopedia filled with various script files. Its about time we use our knowledge of our IDE to as a tool to query our encyclopedia. At this point your encyclopedia will contain the following top-level folders:

```
├─── .vscode
├─── fa-deploy-fafdevelop
├─── lua
├─── maps
├─── mohodata
├─── moholua
```

Open Visual Studio Code and then open the encyclopedia as a folder with `CTRL + K + O`. Then save this folder as a workspace by navigating to `File -> Save workspace as`. You can call the file anything you'd like - just make sure that its in the encyclopedia folder. 

As a small exercise: use the Peacock extension we spoke of earlier to give your workspace a new color. When we start programming we'll have at least two instances of Visual Studio Code open: one with the map scripts and another with the encyclopedia to help us search through files for examples.

### Downloading the map

Download the map from the [repository]() to your map directory. This is typically a different map directory than the maps folder in your installation folder. In my case this is located in `C:\Users\%USER%\Documents\My Games\Gas Powered Games\Supreme Commander Forged Alliance\maps` where `%USER%` is a placeholder for your user account. If the maps folder does not exist you can create one. Unpack the .zip into this directory. We'll use this map as a template to create our own survival. 

All the assets of this repository are licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/). Please read up what the license entails before you continue.

As a small exercise: create a workspace of the map directory and use the `Peacock` extension we spoke of earlier to give your workspace a new color. Make sure it is different from the color you use for your encyclopedia!

