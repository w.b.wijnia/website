+++
title = "Communities"
avatar = "/images/site/community.png"
+++

A well-known community is like walking into your favorite pub. You recognize people and they recognize you. You respect one another and each others ambition and personality regardless whether you agreed with each other during the last discussion.

![preview-2](/images/community/supreme-commander.png)
_[Image](https://forum.faforever.com/topic/6/screenshot-of-the-week?_=1615755039894) by lilSidlil_

I'm in the lucky position to have not one, but two communities where this mutual understanding is upholding. Both communities revolve around [Forged Alliance](https://store.steampowered.com/app/9420/Supreme_Commander_Forged_Alliance/) while still being completely different in both the nature of the community and the game balance that they play.
