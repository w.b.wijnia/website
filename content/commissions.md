+++
title = "Commissions"
avatar = "/images/site/commercial.png"
+++

### Supreme Commander

The game where I've slowly worked on the foundation of what I can achieve today. I have a highly optimized workflow for the format of Supreme Commander. Therefore the price is lower in comparison to other options. The price depends on the size of the map, the number of iterations you wish to have and the (ownership) rights to the map. All prices shown are excluding taxes.

```
Map size            Number of iterations        Ownership rights   
-------------       --------------------        -------------------------------------------------------
05x05 -> €300       4 iterations -> €50         You have the rights: final price is increased with 50%.
10x10 -> €450       6 iterations -> €75         I hold the rights: final price as it is
20x20 -> €600       8 iterations -> €100
40x40 -> € ?
80x80 -> € ?
```

Not all map sizes support map-wide assets. At some point the quality of the map-wide decals start to degrade too much. This requires a fallback to placing decals manually. This is not part of the standard package as the time investment increases significantly. You can contact me directly to get a proper indication of the price for maps of the size 40x40 and beyond.

```
Map size        Map-wide lighting           Map-wide normals            Map-wide albedo     
--------        -----------------           ----------------            ---------------
05x05           Yes - 256x256               Yes - 4096x4096             Yes - 4096x4096
10x10           Yes - 512x512               Yes - 8192x8192             Yes - 8192x8192
20x20           Yes - 1024x1024             Yes - 8192x8192             Yes - 8192x8192
40x40           Yes - 2048x2048             Yes - 8192x8192             No
80x80           Yes - 4096x4096             No                          No
```

### Non-profit projects

I am a huge fan of non-profit projects such as open source games. The greatest ideas can spark in these projects and I'd love to be part of it. Depending on the project there may or may not be a financial compensation. For example, if I'd be able to learn something new by working with your project then I'm willing to drop the financial compensation all together.

If your project is non-profit then do not hesitate to contact me. We can discuss the details via email and come to an agreement we both see fit. There are a few limitations:
 - You must be able to proof to me that your project is indeed non-profit.
 - You must have a clear idea of the technical requirements of the assets you wish me to make for you.

As an example of the former: your project has an open source license on Github since the start of the project. As an example of the latter: I need you to tell me the rendering model of your pipeline. A PBR pipeline will require different assets than an older blinn-phong based pipeline. 

### Commercial projects


