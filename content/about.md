+++
title = "About"
author = "(Jip) Willem Wijnia"
avatar = "/images/site/about.png"
+++

### About me

I am a 26 year old junior-human living in the Netherlands. The journey started years ago by making maps for Halo 2. The only map that I ended up publishing was [Green Hill](http://h2v.halomaps.org/index.cfm?pg=3&fid=2384). At the time I was too scared to publish anything even though I had more assets ready to go. Luckily that has changed since then.

### About what I've done

Today I try and be the person that I would've wanted to meet when I was young. Someone that writes guides on how you can achieve something. Someone who's open for discussion. Someone who's active in the community and tries to bring people together to make a more friendly environment. Someone who's willing to share his assets to other people that just want to create content for their favorite game.

### About my aim for the future
